import csv

# Open the CSV file and read the contents
with open("rektlist.csv") as csvfile:
    data = csv.reader(csvfile)
    out = ""
    next(data)

    # Loop through each row in the CSV file
    for row in data:
        # Print the HTML table row
        out += "<tr>"
        out += "<td>{}</td>".format(row[0])
        # Determine display class based on status
        cl = "ok"
        if "yes" in row[1]:
            cl = "rekt"
        if row[1] == "almost":
            cl = "meh"
        out +="<td class={}>{}</td>".format(cl, row[1])
        out +="</tr>\n"
    
with open("index-tpl.html") as template:
    contents = template.read()
    print(contents.replace("$TABLE", out))

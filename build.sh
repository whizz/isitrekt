#!/bin/sh

mkdir -p output
rm -rf output/*
python3 gen.py > output/index.html
cp -R static/* output/
